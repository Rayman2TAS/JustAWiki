Where multiple addresses seem to be updated to the same value each frame, the first instance is used.

Type | Address E | Address U | Description
--:|--:|--:|---
`1hxB` | `0x043921` | `0x043921` | Takes the value 0x45 or 0x55 ("E" or "U" in ASCII) soon after the Ubisoft splash loads, matching ROM region. Possibly a coincidence.
`1siB` | `0x01A8BD` | `0x01A45D` | Screen offset (left)
`1siB` | `0x01A8BF` | `0x01A45F` | Screen offset (right)
`1siB` | `0x01A8D1` | `0x01A471` | Screen offset (top)
`1siB` | `0x01A8D3` | `0x01A473` | Screen offset (bottom)
`2bfB` | `0x0CF480` | `0x0CF590` | Controller input (buttons); flags (set = pressed): A, B, Z, Start, D Up, D Down, D Left, D Right, unknown/unassigned, unknown/unassigned, L, R, C Up, C Down, C Left, C Right
`1siB` | `0x0CF482` | `0x0CF592` | Controller input (analog X)
`1siB` | `0x0CF483` | `0x0CF593` | Controller input (analog Y)
`4flB` | `0x0F4A80` | `0x0F4B90` | Oxygen (x1/10 second?)
`1uiB` | `0x1BC54D` | `0x1BC64D` | (Current) HP
`1uiB` | `0x1BC54E` | `0x1BC64E` | Max. HP
`1uiB` | `0x1F1002` | `0x1F1103` | Ly race time