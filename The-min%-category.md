The goal in min% is simple: fail spectacularly.

tl;dr: you can win the game at 55.0% (by the save file progress, 52.0% if you include cages), which is 550/1000 yellow lums and 15/100 cages. Bad-end% glitchless can be done w/ 113 lums (100 + SoWaI + CoBD) and 7 cages.

This is the current minimum score (TAS, RTA + savestates), and also the theoretical minimum score for this category glitchless, as you need at least 550 yellow lums to enter the Iron Mountains. The cage score could possibly be dropped, though not beyond 12 (first lum, Ssssam and ten Teensies) glitchless. Again, this would not change the progress displayed on the save select screen.

The actual minimum number of lums for each level is much lower than this 550-lum limit. In a casual playthrough, players may find they haven't been thorough enough and be forced to complete one of Ly's races, but the number of lums forced upon you is much lower than how many are placed directly in your path.

The current breakdown of forced lums and cages (glitchless) is as follows:

Level | Lums | Cages
--:|---|---
AtB | - | -
WoL | 5/5 | 2/2
FG | 4/50 | 1/7
MoA | 0/50 | 2/5
Bayou | 4/50 | 2/7
Lyfe | 0/50 | -
SoWaI | 12/50 | 0/2
MH | 1/50 | 0/8
CoBD | 1/50 | -
Canopy | 3/50 | 1/4
WB | 6/50 | 2/4
SoSaF | 1/50 | 0/8
EC | 0/50 | 1/5
Press | 8/50 | 1/6
TotW | 1/50 | 0/2
SoRaL-A | 1/50 | 2/7
WoP | 0/50 | -
SoRaL-B | 0/50 | 0/4
TotA | 0/50+1 | 1/6
IM | 0/50 | 0/3
PS | 0/94 | -

Forced lum locations:
* WoL: All 5 are required to leave
* FG: 1 above the bouncy mushroom in the spawn area; 1 at the top of the rope ladder in LZ3, and 2 between the pipes you need to climb to get to LZ4
* Bayou: 1, the first on the diving board where the switch is in LZ0; 1, the last of the line leading down from the log after the barrel ride in LZ0; 2, one on each of the highest two trampolines at the end of LZ1
* SoWaI: 3, the last 3 outside the cave in LZ0; 4 on the rope ladder in said cave; 5 as a 5x beside the barrel resupply
* MH: 1, the first in the last shell ride in LZ3
* CoBD: 1 in the yellow orb fetch quest at the end LZ0 (TODO where)
* Canopy: 1 in LZ2 during the entrance cutscene; 2 from the Teensy's cage
* WB: 5 as a 5x from the purple lum's cage in LZ0; 1, the first in the cave where the Teensy's cage is
* SoSaF: 1 on Umber's head
* Press: 1 just before the first cage in LZ0; 1 just after said cage; 4, the first, third, fourth, and fifth during the tower climb in LZ1; 2, the last two on the crane at the top of the tower
* TotW: 1, the second lum in the first segment
* SoRaL-A: 1 at the start of LZ2

Forced cage locations:
* WoL: 1 w/ the first lum, 1 w/ the Teensies
* FG: 1 w/ a Teensy
* MoA: 1 w/ Ssssam, 1 w/ a Teensy
* Bayou: 1 w/ a purple lum, 1 w/ a Teensy
* Canopy: 1 w/ a Teensy
* WB: 1 w/ a purple lum and a 5x in LZ0, 1 w/ a Teensy
* EC: 1 w/ a Teensy
* Press: 1 w/ a Teensy
* SoRaL-A: 1 w/ a purple lum in LZ0, 1 w/ a Teensy
* TotA: 1 w/ a Teensy

Optional areas:
* Should you return to FG via EC, the cage is skippable.
* Should you enter Lyfe, every lum is skippable (double-check required), though whether you can go slightly out of your way fifty times and never let the time expire is something else.
* Should you enter SoSaF LZ2, it is currently unknown how many lums are skippable.
* Should you enter WoP, every lum is skippable.
* Should you detour in TotA LZ0, the "thousandth lum" is skippable.
* Should you enter TotA LZ2, every lum and cage is skippable.